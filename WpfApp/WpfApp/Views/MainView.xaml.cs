﻿using System.Windows;
using WpfApp.Buisness;
using WpfApp.ViewModels;

namespace WpfApp.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            IBusiness business = new Business();
            IMainViewModel mainvm = new MainViewModel(business);

            this.DataContext = mainvm;
            InitializeComponent();
        }
      
    }
}
