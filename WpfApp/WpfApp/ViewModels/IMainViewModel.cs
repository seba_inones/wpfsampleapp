﻿using System.Collections.ObjectModel;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    public interface IMainViewModel
    {
        void Incrementar();
        void AgregarPersona();
        ObservableCollection<Person> Persons { get; }
    }
}
