﻿using Caliburn.Micro;
using System.Collections.ObjectModel;
using System.Linq;
using WpfApp.Buisness;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    public class MainViewModel : PropertyChangedBase, IMainViewModel
    {
        public ObservableCollection<Person> Persons { get; set; } = new ObservableCollection<Person>();

        private readonly IBusiness business = null;

        private Person selectedPerson;

        public Person SelectedPerson
        {
            get { return selectedPerson; }
            set
            {
                selectedPerson = value;
                NotifyOfPropertyChange(() => SelectedPerson);
            }
        }

        public MainViewModel(IBusiness business)
        {

            this.business = business;

            if(this.business.DoSomething())
            {
                Persons.Add(new Person { Age = 5, Name = "Malena", SurName = "Muller" });
                Persons.Add(new Person { Age = 36, Name = "Seba", SurName = "Iñones" });
            }
            
        }

        public int LastAge
        {
            get { return Persons.Last().Age; }
            set
            {
                Persons.Last().Age = value;
                NotifyOfPropertyChange(() => LastAge);
            }
        }

        public bool CanIncrementar
        {
            get { return LastAge <= 38; }
        }

        public void Incrementar()
        {
            LastAge++;
            NotifyOfPropertyChange(() => CanIncrementar);
        }

        public void AgregarPersona()
        {
            var newPerson = new Person() { Name = "Andrea", Age = 35 };

            if (!Persons.Any(p => p.Name == newPerson.Name))
            {
                Persons.Add(newPerson);
            }
        }
    }

}
