﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp.Models
{
    public class Person
    {
        public int Age { get; set; }

        public string Name { get; set; }

        public string SurName { get; set; }

    }
}
