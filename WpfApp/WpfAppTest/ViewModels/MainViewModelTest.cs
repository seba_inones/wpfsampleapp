﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfApp.ViewModels;
using WpfApp.Buisness;
using Moq;

namespace WpfAppTest
{
    [TestClass]
    public class MainViewModelTest
    {
        [TestMethod]
        public void WhenAgregarPersonaAndAlreadyExistThenPersonaIsNotAdded()
        {


            var mockedBuisness = new Mock<IBusiness>();
            mockedBuisness.Setup(bus => bus.DoSomething()).Returns(true);

            //Arrange
            //IBusiness notMockedBusiness = new Business();
            IMainViewModel sut = new MainViewModel(mockedBuisness.Object);


            //Act

            sut.AgregarPersona();
            int expectedCount = sut.Persons.Count;
            sut.AgregarPersona();

            //Assert
            Assert.AreEqual(expectedCount, sut.Persons.Count);
        }


        [TestMethod]
        public void GivenPreconditionWhenPerfomingSomeActionThenAssertSomeBehavior()
        {
            //Arrange.

            //Act.

            //Assert.                   
        }
    }
}
